let express = require('express');
let app = express();
let url = require('url');

app.use(express.static('public'));

let data = require('./public/users/info');
app.get('/', function (req, res) {
    let query = url.parse(req.url, true).query;

    let sort = query['sort'] || '';

    let page = parseInt(query['page']) || 0;
    let limit = parseInt(query['limit']) || 30;


    let fi = page * limit;
    let li = (page + 1) * limit;

    let s = sort.split(",", 2);

    let users = data.list.users;
    let groups = data.list.groups;

    let field = s[0];
    let direction = s[1];
    users = users.sort((a, b) => {
        if (a[field] < b[field]) return direction === 'desc' ? 1 : -1;
        if (a[field] > b[field]) return direction === 'desc' ? -1 : 1;
        return 0;
    });


    let output = users.slice(fi, li);

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type')
    res.header('Content-Type', 'application/json')

    res.json({groups: groups, users: output});

});

app.listen(3000, function () {


    console.log('Сервер запущен по адресу localhost:3000!');
    console.log('Получение JSON-файла по ссылке http://localhost:3000/users/info.json');
});